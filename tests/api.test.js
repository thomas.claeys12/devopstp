// test.js
const { returnHello } = require('../src/main.js');

jest.mock('../src/main.js', () => ({
  returnHello: jest.fn(),
}));

describe('mock /hello function', () => {
  test('returns hello world', () => {
    // Mock the `returnHello` function to return "hello world"
    returnHello.mockReturnValue('Hello Sciences-U !');

    // Call the mocked `hello` function and expect it to return "hello world"
    expect(returnHello()).toEqual('Hello Sciences-U !');
  });
});

import express from "express";

const app = express();

app.get("/hello", (req, res) => {
  return res.send(returnHello());
});

export const returnHello = () => {
    return "Hello Sciences-U !";
}

app.listen(3000);
